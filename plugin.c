/*
 * TeamSpeak 3 demo plugin
 *
 * Copyright (c) 2008-2014 TeamSpeak Systems GmbH
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "include/public_errors.h"
#include "include/public_errors_rare.h"
#include "include/public_definitions.h"
#include "include/public_rare_definitions.h"
#include "include/ts3_functions.h"
#include "plugin.h"

#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <inttypes.h>
#include <sys/time.h>

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/nanohttp.h>

static struct TS3Functions ts3Functions;

#define _strcpy(dest, destSize, src) { strncpy(dest, src, destSize-1); (dest)[destSize-1] = '\0'; }

#define PLUGIN_API_VERSION 20

#define PATH_BUFSIZE 512
#define COMMAND_BUFSIZE 128
#define INFODATA_BUFSIZE 128
#define SERVERINFO_BUFSIZE 256
#define CHANNELINFO_BUFSIZE 512
#define RETURNCODE_BUFSIZE 128


//Eigene Defines
#define MIN_0(a,b) (a <= 0 || b <= 0 ? 0 : (a > b ? b : a))

#define MAX_SIZE_TITLE 64
#define MAX_SIZE_INTERPRET 64
#define MAX_SIZE_ALBUM 64

#define ERROR_XML_NO_CONNECTION 1
#define ERROR_XML_STREAM_ENDED 2
#define ERROR_XML_CREATE_PARSER 3
#define ERROR_XML_NOT_WELLFORMED 4
#define ERROR_XML_STREAM_ERROR 5

#define NICK_NAME_PREFIX "[Musik]"

#define POLL_INTERVAL 10

#define XML_BUFFER_SIZE 4096

#ifndef VLC_HTTP_PASSWD_BASE64
	#define VLC_HTTP_PASSWD_BASE64 "Om11c2ljYm90" // ":musicbot"
#endif

#ifndef __DEBUG
	#define __DEBUG 0
#endif

#define LOG_LEVEL_0 0
#define LOG_LEVEL_1 1
#define LOG_LEVEL_2 2
#define LOG_LEVEL_3 3

//uint64 srvConHandlerID;

static unsigned int plugin_running;
static unsigned int plugin_status;

static pthread_t worker;

static pthread_mutex_t mutex_connected = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t cond_connected = PTHREAD_COND_INITIALIZER;

static pthread_mutex_t mutex_plug_run = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t cond_plug_run = PTHREAD_COND_INITIALIZER;

static void *worker_thread(void *);
static int get_status(char *,char *, char *, struct timespec *);
static void build_nickname_and_description(char *, char *, char *, char *, char *);
static int set_nick_and_descr(uint64, char *, char *);


/*********************************** Required functions ************************************/
/*
 * If any of these required functions is not implemented, TS3 will refuse to load the plugin
 */

/* Unique name identifying this plugin */
const char *ts3plugin_name() {
	return "Musicbot nick-changer";
}

/* Plugin version */
const char *ts3plugin_version() {
	return "0.1";
}

/* Plugin API version. Must be the same as the clients API major version, else the plugin fails to load. */
int ts3plugin_apiVersion() {
	return PLUGIN_API_VERSION;
}

/* Plugin author */
const char *ts3plugin_author() {
	/* If you want to use wchar_t, see ts3plugin_name() on how to use */
	return "Patrick Werneck";
}

/* Plugin description */
const char *ts3plugin_description() {
	/* If you want to use wchar_t, see ts3plugin_name() on how to use */
	return "This plugin changes the nickname and the client-description to the current title in VLC. The HTTP remote interface of VLC must be turned on.";
}

/* Set TeamSpeak 3 callback functions */
void ts3plugin_setFunctionPointers(const struct TS3Functions funcs) {
	ts3Functions = funcs;
}

/*
 * Custom code called right after loading the plugin. Returns 0 on success, 1 on failure.
 * If the function returns 1 on failure, the plugin will be unloaded again.
 */
int ts3plugin_init() {
	/* Your plugin init code here */
	#if __DEBUG >= LOG_LEVEL_1
	printf("[PLUGIN-VLC][INFO] init plugin\n");
	#endif
	
	plugin_running = 1;
	if(pthread_create(&worker, NULL, &worker_thread, NULL) != 0){
		perror("[PLUGIN-VLC]");
		return 1;
	}

	return 0;
	/* 0 = success, 1 = failure, -2 = failure but client will not show a "failed to load" warning */
	/* -2 is a very special case and should only be used if a plugin displays a dialog (e.g. overlay) asking the user to disable
	 * the plugin again, avoiding the show another dialog by the client telling the user the plugin failed to load.
	 * For normal case, if a plugin really failed to load because of an error, the correct return value is 1. */
}

/* Custom code called right before the plugin is unloaded */
void ts3plugin_shutdown() {
	/* Your plugin cleanup code here */
	#if __DEBUG >= LOG_LEVEL_1
	printf("[PLUGIN-VLC][INFO] shutdown\n");
	#endif
	
	//setting plugin_running atomical to 0
	__sync_fetch_and_and(&plugin_running, 0);
	
	//wake up thread if its waiting to update status
	pthread_mutex_lock(&mutex_plug_run);
	pthread_cond_broadcast(&cond_plug_run);
	pthread_mutex_unlock(&mutex_plug_run);
	
	//wake up thread if its ts is not connected to a server
	pthread_mutex_lock(&mutex_connected);
	pthread_cond_broadcast(&cond_connected);
	pthread_mutex_unlock(&mutex_connected);
	
	
	if(pthread_join(worker, NULL) != 0){
		perror("[PLUGIN-VLC]");
	}	
	#if __DEBUG >= LOG_LEVEL_1
	else{
		printf("[PLUGIN-VLC][INFO] worker joined shutdown thread\n");
	}
	#endif	
	/*
	 * Note:
	 * If your plugin implements a settings dialog, it must be closed and deleted here, else the
	 * TeamSpeak client will most likely crash (DLL removed but dialog from DLL code still open).
	 */

}

/****************************** Optional functions ********************************/
/*
 * Following functions are optional, if not needed you don't need to implement them.
 */

/*
 * Plugin requests to be always automatically loaded by the TeamSpeak 3 client unless
 * the user manually disabled it in the plugin dialog.
 * This function is optional. If missing, no autoload is assumed.
 */
int ts3plugin_requestAutoload() {
	return 1;  /* 1 = request autoloaded, 0 = do not request autoload */
}

/************************** TeamSpeak callbacks ***************************/
/*
 * Following functions are optional, feel free to remove unused callbacks.
 * See the clientlib documentation for details on each function.
 */

/* Clientlib */

void ts3plugin_onConnectStatusChangeEvent(uint64 serverConnectionHandlerID, int newStatus, unsigned int errorNumber) {
	/* Some example code following to show how to use the information query functions. */
	pthread_mutex_lock(&mutex_connected);
	plugin_status = newStatus;
	if (newStatus == STATUS_CONNECTION_ESTABLISHED) { /* connection established and we have client and channels available */		
		pthread_cond_broadcast(&cond_connected);
		#if __DEBUG >= LOG_LEVEL_1
		printf("[PLUGIN-VLC][INFO] send broadcast to cond_plug_run (serverConnectionHandlerID = %" PRIu64 ")\n",  serverConnectionHandlerID);
		#endif
	}	
	pthread_mutex_unlock(&mutex_connected);
}

void* worker_thread(void* ptr){
	#if __DEBUG >= LOG_LEVEL_1
	printf("[PLUGIN-VLC][INFO] worker_started\n");
	#endif	
	int returncode;
	char title[MAX_SIZE_TITLE], interpret[MAX_SIZE_INTERPRET], album[MAX_SIZE_ALBUM];
	char nick_buffer[TS3_MAX_SIZE_CLIENT_NICKNAME_NONSDK], descr_buffer[TS3_MAX_SIZE_CLIENT_DESCRIPTION];
	struct timespec remaining_time;
	struct timeval curr_time;
	
	memset(&remaining_time, 0, sizeof(struct timespec));
	
	while(__sync_fetch_and_or(&plugin_running, 0)){
		pthread_mutex_lock(&mutex_connected);
		//wait if not already connected
		if(plugin_status != STATUS_CONNECTION_ESTABLISHED){
			pthread_cond_wait(&cond_connected, &mutex_connected);
			while(plugin_status != STATUS_CONNECTION_ESTABLISHED && __sync_fetch_and_or(&plugin_running, 0)){
				#if __DEBUG >= LOG_LEVEL_1
				printf("[PLUGIN-VLC][INFO] woke up, but not connected, waiting again\n");
				#endif
				pthread_cond_wait(&cond_connected, &mutex_connected);	
			}
		}			
		pthread_mutex_unlock(&mutex_connected);
		
		while(__sync_fetch_and_or(&plugin_running, 0)){
			//if woke up and plugin still running, then remaining_time is elapsed and we have to update the status

			if((returncode = get_status(title, interpret, album, &remaining_time)) != ERROR_ok){
				fprintf(stderr, "[PLUGIN-VLC][ERROR] could not get status update: get_status returned 0x%x\n", returncode);
				return NULL;
			}
			#if __DEBUG >= LOG_LEVEL_1
			printf("[PLUGIN-VLC][INFO] received status update\n");
			#endif
			
			build_nickname_and_description(nick_buffer, descr_buffer, title, interpret, album);
			if((returncode = set_nick_and_descr(ts3Functions.getCurrentServerConnectionHandlerID(), nick_buffer, descr_buffer)) != ERROR_ok){
				fprintf(stderr, "[PLUGIN-VLC][ERROR] failed to set nick and description with error 0x%x\n", returncode);
				//NOTE: maybe shutdown the plugin
			}
			
			#if __DEBUG >= LOG_LEVEL_2
			printf("[PLUGIN-VLC][INFO] trying to sleep for %" PRIu64 " seconds\n", (uint64) POLL_INTERVAL);//remaining_time.tv_sec);
			#endif
			
			//NOTE: possible to build upon song time with += remaining_time.tv_sec (be carefull if it is low)
			gettimeofday(&curr_time, NULL);
			remaining_time.tv_sec = curr_time.tv_sec + POLL_INTERVAL;
			
			
			
			pthread_mutex_lock(&mutex_plug_run);
			returncode= pthread_cond_timedwait(&cond_plug_run, &mutex_plug_run, &remaining_time);
			pthread_mutex_unlock(&mutex_plug_run);
			
			#if __DEBUG >= LOG_LEVEL_2
			if(returncode == ETIMEDOUT){
				printf("[PLUGIN-VLC][INFO] timed out without getting shutdown signal\n");
			}
			#endif
			
			pthread_mutex_lock(&mutex_connected);
			if(plugin_status != STATUS_CONNECTION_ESTABLISHED){
				pthread_mutex_unlock(&mutex_connected);
				#if __DEBUG >= LOG_LEVEL_1
				printf("[PLUGIN-VLC][INFO] no longer connected, waiting for connect again\n");
				#endif
				break;
			}else{				
				pthread_mutex_unlock(&mutex_connected);
			}
			
		}
		
	}
	
	return NULL;
}


int get_status(char *title,char *interpret, char *album, struct timespec *remaining_time){
	xmlParserCtxtPtr ctxt = NULL;
	xmlDocPtr doc = NULL;
	char buffer[XML_BUFFER_SIZE+1];
	int read_bytes;
	int returncode = ERROR_ok;
	
	
	void *ctx = xmlNanoHTTPMethod("http://localhost:8080/requests/status.xml", "GET", "", NULL, "Authorization: Basic " VLC_HTTP_PASSWD_BASE64 "\r\n", 0);
	if(ctx == NULL){
		fprintf(stderr, "[PLUGIN-VLC][ERROR] could not open connection\n");
		returncode = ERROR_XML_NO_CONNECTION;
		goto get_status_cleanup;
	}
	
	read_bytes = xmlNanoHTTPRead(ctx, buffer, XML_BUFFER_SIZE);
	if(read_bytes == 0){
		fprintf(stderr, "[PLUGIN-VLC][ERROR] stream ended before any data could be read\n");
		returncode = ERROR_XML_STREAM_ENDED;
		goto get_status_cleanup;
	}else if (read_bytes < 0){
		fprintf(stderr, "[PLUGIN-VLC][ERROR] stream error\n");
		returncode = ERROR_XML_STREAM_ERROR;
		goto get_status_cleanup;
	}
	
	ctxt = xmlCreatePushParserCtxt(NULL, NULL, buffer, read_bytes, NULL);
	if(ctxt == NULL){
		fprintf(stderr, "[PLUGIN-VLC][ERROR] failed to create parser context\n");
		returncode = ERROR_XML_CREATE_PARSER;
		goto get_status_cleanup;
	}
	
	while((read_bytes = xmlNanoHTTPRead(ctx, buffer, XML_BUFFER_SIZE)) > 0){
		xmlParseChunk(ctxt, buffer, read_bytes, 0);
		#if __DEBUG >= LOG_LEVEL_2
		printf("[PLUGIN-VLC][INFO] read additional bytes from stream\n");
		#endif
	}
	
	//no further input
	xmlParseChunk(ctxt, buffer, 0, 1);
		
	doc = ctxt->myDoc;
	if(!ctxt->wellFormed){
		fprintf(stderr, "[PLUGIN-VLC][ERROR] xml is not well-formed\n");
		returncode = ERROR_XML_NOT_WELLFORMED;
	}
	
	memset(remaining_time, 0, sizeof(struct timespec));	
	
	#if __DEBUG >= LOG_LEVEL_2
	printf("[PLUGIN][INFO] parsed xml successfully\n");
	#endif
	xmlNode *root = xmlDocGetRootElement(doc);
	
	xmlChar *entry;
	xmlNode *curr = root->xmlChildrenNode;
	xmlNode *info, *meta;
	int played_time = 0, total_time = 0;
	int title_missing_flag = 1, interpret_missing_flag = 1, album_missing_flag = 1;
	while(curr){
		if(!xmlStrcmp(curr->name, (xmlChar*) "time")){			
			entry = xmlNodeListGetString(doc, curr->xmlChildrenNode, 1);
			played_time = atoi((const char*)entry);
			#if __DEBUG >= LOG_LEVEL_3
			printf("[PLUGIN-VLC][INFO] XML played time found - %d\n", played_time);
			#endif
			xmlFree(entry);
		}else if(!xmlStrcmp(curr->name, (xmlChar*) "length")){
			entry = xmlNodeListGetString(doc, curr->xmlChildrenNode, 1);
			total_time = atoi((const char*)entry);
			#if __DEBUG >= LOG_LEVEL_3
			printf("[PLUGIN-VLC][INFO] XML total time found - %d\n", total_time);
			#endif
			xmlFree(entry);	
		}else if(!xmlStrcmp(curr->name, (xmlChar*) "information")){
			info = curr->xmlChildrenNode;
			while(info){
				if(!xmlStrcmp(info->name, (xmlChar*) "category") ){
					entry = xmlGetProp(info, (xmlChar*) "name");
					if(entry && !xmlStrcmp(entry, (xmlChar*) "meta")){
						#if __DEBUG >= LOG_LEVEL_3
						printf("[PLUGIN-VLC][INFO] XML <category name=\"%s\"> node found\n", entry);
						#endif
						//free entry i reuse the pointer in the following while, and do not need it any longer
						xmlFree(entry);
						
						meta = info->xmlChildrenNode;
						#if __DEBUG >= LOG_LEVEL_3
						xmlChar *ptr = xmlNodeListGetString(doc, meta, 1);
						printf("[PLUGIN-VLC][INFO] XML <info name=\"%s\"> node found\n", entry);
						xmlFree(ptr);
						#endif
						while(meta){
							if(!xmlStrcmp(meta->name, (xmlChar*) "info") ){
								entry = xmlGetProp(meta, (xmlChar*) "name");
								#if __DEBUG >= LOG_LEVEL_3
								printf("[PLUGIN-VLC][INFO] XML <info name=\"%s\"> node found\n", entry);
								#endif
								if(entry && !xmlStrcmp(entry, (xmlChar*) "title")){
									xmlFree(entry);
									entry = xmlNodeListGetString(doc, meta->xmlChildrenNode, 1);
									strncpy(title, (char*) entry, MAX_SIZE_TITLE);		
									title_missing_flag = 0;
									#if __DEBUG >= LOG_LEVEL_3
									printf("[PLUGIN-VLC][INFO] XML title found - %s\n", title);
									#endif
									xmlFree(entry);
								}else if(entry && !xmlStrcmp(entry, (xmlChar*) "album")){
									xmlFree(entry);									
									entry = xmlNodeListGetString(doc, meta->xmlChildrenNode, 1);
									strncpy(album, (char*) entry, MAX_SIZE_ALBUM);
									album_missing_flag = 0;
									#if __DEBUG >= LOG_LEVEL_3
									printf("[PLUGIN-VLC][INFO] XML album found - %s\n", album);
									#endif
									xmlFree(entry);
								}else if(entry && !xmlStrcmp(entry, (xmlChar*) "artist")){
									xmlFree(entry);
									entry = xmlNodeListGetString(doc, meta->xmlChildrenNode, 1);
									strncpy(interpret, (char*) entry, MAX_SIZE_INTERPRET);
									interpret_missing_flag = 0;
									#if __DEBUG >= LOG_LEVEL_3
									printf("[PLUGIN-VLC][INFO] XML artist found - %s\n", interpret);
									#endif
									xmlFree(entry);
								}else if(entry){
									#if __DEBUG >= LOG_LEVEL_3
									xmlFree(entry);									
									entry = xmlNodeListGetString(doc, meta->xmlChildrenNode, 1);
									printf("[PLUGIN-VLC][INFO] content: %s\n", entry);
									#endif
									xmlFree(entry);
								}
							}
							meta=meta->next;
						}
					}else if(entry){
						xmlFree(entry);
					}
				}
				info = info->next;
			}				
		}
		curr = curr->next;
	}
	remaining_time->tv_sec = total_time - played_time <= 0 ? 0 : total_time - played_time;
	remaining_time->tv_nsec = 0;
	if(title_missing_flag){
		#if __DEBUG >= LOG_LEVEL_1
		printf("[PLUGIN-VLC][INFO] no title found - setting unkown\n");
		#endif
		strcpy(title, "unknown");
	}
	if(album_missing_flag){
		#if __DEBUG >= LOG_LEVEL_1
		printf("[PLUGIN-VLC][INFO] no album found - setting unkown\n");
		#endif
		strcpy(album, "unknown");
	}
	if(interpret_missing_flag){
		#if __DEBUG >= LOG_LEVEL_1
		printf("[PLUGIN-VLC][INFO] no interpret found - setting unkown\n");
		#endif
		strcpy(interpret, "unknown");
	}	
	
	get_status_cleanup:
		if(ctx)
			xmlNanoHTTPClose(ctx);
		if(ctxt)
			xmlFreeParserCtxt(ctxt);
		if(doc)
			xmlFreeDoc(doc);
	
	return returncode;	
}


void build_nickname_and_description(char *nick_buffer, char *descr_buffer, char *title, char *interpret, char *album){
	size_t title_len = strlen(title);
	size_t interpret_len = strlen(interpret);
	size_t album_len = strlen(album);
	
	size_t nick_ctr = TS3_MAX_SIZE_CLIENT_NICKNAME_NONSDK;
	size_t descr_ctr = TS3_MAX_SIZE_CLIENT_DESCRIPTION;
	
	size_t add;
	
	nick_buffer[0] = '\0';
	descr_buffer[0] = '\0';
	
	add = MIN_0(strlen(NICK_NAME_PREFIX), nick_ctr);
	strncat(nick_buffer, NICK_NAME_PREFIX, add);
	nick_ctr -= add;
	
	add = MIN_0(title_len, nick_ctr) ;
	strncat(nick_buffer, title, add);
	nick_ctr -= add;
	
	add = MIN_0(title_len, descr_ctr);
	strncat(descr_buffer, title, add);
	descr_ctr -= add;
	
	add = MIN_0(3, nick_ctr) ;
	strncat(nick_buffer, " - ", add);
	nick_ctr -= add;
	
	add = MIN_0(4, descr_ctr);
	strncat(descr_buffer, " by ", add);
	descr_ctr -= add;
	
	add = MIN_0(interpret_len, nick_ctr) ;
	strncat(nick_buffer, interpret, add);
	nick_ctr -= add;
	
	add = MIN_0(interpret_len, descr_ctr);
	strncat(descr_buffer, interpret, add);
	descr_ctr -= add;
	
	add = MIN_0(3, nick_ctr) ;
	strncat(nick_buffer, " - ", add);
	nick_ctr -= add;
	
	add = MIN_0(4, descr_ctr);
	strncat(descr_buffer, " on ", add);
	descr_ctr -= add;
	
	add = MIN_0(album_len, nick_ctr) ;
	strncat(nick_buffer, album, add);
	nick_ctr -= add;
	
	add = MIN_0(album_len, descr_ctr);
	strncat(descr_buffer, album, add);
	descr_ctr -= add;	
}

int set_nick_and_descr(uint64 serverConnectionHandlerID, char *nick, char *description){
	unsigned int returncode;
	char dont_care;
	
	//Cut off too long nicks and descriptions
	if(strlen(nick) >= TS3_MAX_SIZE_CLIENT_NICKNAME_NONSDK){
		nick[TS3_MAX_SIZE_CLIENT_NICKNAME_NONSDK] = '\0';
		#if __DEBUG >= LOG_LEVEL_1
		printf("[PLUGIN-VLC][INFO] client_nickname too long. Cut off after %u signs\n", TS3_MAX_SIZE_CLIENT_NICKNAME_NONSDK);
		#endif
	}
	if(strlen(description) >= TS3_MAX_SIZE_CLIENT_DESCRIPTION){
		description[TS3_MAX_SIZE_CLIENT_DESCRIPTION] = '\0';		
		#if __DEBUG >= LOG_LEVEL_1
		printf("[PLUGIN-VLC][INFO] client_description too long. Cut off after %u signs\n", TS3_MAX_SIZE_CLIENT_DESCRIPTION);
		#endif
	}
		
	anyID my_id;
	if((returncode = ts3Functions.getClientID(serverConnectionHandlerID, &my_id)) != ERROR_ok){
		fprintf(stderr, "[PLUGIN-VLC][ERROR] failed getting clientID  %" PRIu64 "\n", serverConnectionHandlerID);
		return returncode;
	}
	#if __DEBUG >= LOG_LEVEL_1
	printf("[PLUGIN-VLC][INFO] successfully got clientID\n");
	#endif
	
	char *old_nick;
	if((returncode = ts3Functions.getClientSelfVariableAsString(serverConnectionHandlerID, CLIENT_NICKNAME, &old_nick)) != ERROR_ok){
		fprintf(stderr, "[PLUGIN-VLC][ERROR] failed getting nickname\n");
		return returncode;		
	}
	
	#if __DEBUG >= LOG_LEVEL_1
	printf("[PLUGIN-VLC][INFO] current nick \"%s\" requested nick \"%s\"\n",old_nick, nick);
	#endif
	
	//prevent overwriting nick with the same name, which causes problems
	if(strcmp(old_nick, nick)){		
		if ((returncode = ts3Functions.setClientSelfVariableAsString(serverConnectionHandlerID, CLIENT_NICKNAME, nick)) != ERROR_ok) {
			fprintf(stderr, "[PLUGIN-VLC][ERROR] failed setting nickname\n");
			return returncode;
		}
		#if __DEBUG >= LOG_LEVEL_1
		printf("[PLUGIN-VLC][INFO] successfully set nickname \"%s\"\n", nick);
		#endif
	}
	#if __DEBUG >= LOG_LEVEL_1
	else{
		printf("[PLUGIN-VLC][INFO] nick not changed\n");
	}
	#endif
	

	returncode = ts3Functions.requestClientEditDescription(serverConnectionHandlerID, my_id, description, &dont_care);
	if (!(returncode == ERROR_ok || returncode == ERROR_ok_no_update)) {
		fprintf(stderr, "[PLUGIN-VLC][ERROR] failed setting description\n");
		return returncode;
	}
	#if __DEBUG >= LOG_LEVEL_1
	if(returncode == ERROR_ok)
		printf("[PLUGIN-VLC][INFO] successfully set description \"%s\"\n", description);
	else if(returncode == ERROR_ok_no_update)
		printf("[PLUGIN-VLC][INFO] description not changed, no update performed\"%s\"\n", description);
	#endif

	returncode = ts3Functions.flushClientSelfUpdates(serverConnectionHandlerID, &dont_care);
	if (!(returncode == ERROR_ok || returncode == ERROR_ok_no_update)) {
		fprintf(stderr, "[PLUGIN-VLC][ERROR] failed flushing updates\n");
		return returncode;
	} 
	#if __DEBUG >= LOG_LEVEL_1
	if(returncode == ERROR_ok)
		printf("[PLUGIN-VLC][INFO] flushed changes\n");
	else if(returncode == ERROR_ok_no_update)
		printf("[PLUGIN-VLC][INFO] no flush needed, nothing changed\n");
	#endif

	
	return ERROR_ok;
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	




