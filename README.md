#INFO#
####Quick Summary####
Main purpose of this Repo is a teamspeak3 plugin, which changes the client-nickname and description to the current title playing in VLC. It also provides a startscript for both, the teamspeak3-client and VLC media player with the http-interface as an additional interface.

####Version####
0.1


#SETUP#
####Prerequisites####
+ Working teamspeak3-client (API version >= 20)  installation [may work for lower versions, too]
+ Working VLC (>= 2.0.0) installation with the http-interface running [may work for lower versions, too]
+ Working Audio	Loopback from VLC to teamspeak3-client. See *AUDIO LOOPBACK SETUP* for setup information.

####Dependencies####
+ libxml2
+ POSIX Threads

####Configuration####
All configuration changes are in the `config` file
	
1. Change `TS3_ROOT_PATH` in `line 7` to the top level directory of your teamspeak3 client installation (mandatory for automatic installation)	
2. Change `TS3_SERVER_ADDR` in `line 10` to the serveraddress the musicbot-client should connect to	
3. If the server runs on a different port than `9987` change `TS3_SERVER_PORT`
4. Optionally set the teamspeak3 serverpassword and join Channel in `line 16, 19`
5. Set the password the vlc http-control-interface is protected with. If this is left blank password is set to `musicbot`

####Deployment instructions####
1. Run configured config script with `./config`. It creates a makefile and a musicbot startscript.
2. Run `make` to compile and install the plugin properly
3. Possibly enable the plugin in teamspeak3 in `Settings -> Plugins -> Musicbot nick-changer`

####Additional Makefile instructions####
+ `make compile`: Just compiles the plugin
+ `make install`: compiles the plugin if not done already and copy it to the correct teamspeak plugin directory
+ `make clean`: remove all files created by `make compile`
+ `make clear-config`: remove all files created by `./config`


# AUDIO LOOPBACK SETUP (archlinux)#
####ALSA based loopback####
1. ALSA module is shipped with the default archlinux kernel, so there is no need to manually install it
2. Install the package `alsa-utils`
3. Load the kernel module `snd_aloop` with the command `modprobe snd_aloop` and set it to be automatically loaded at systemstart with the command `echo "snd_aloop" > /etc/modules-load.d/snd_aloop.conf`. This module creates two virtual loopback soundcards
4. Open the file `/etc/asound.conf` for global configuration or `~/.asound.conf` for user-configuration and insert the content of the box below. Possilby you have to change `pcm "hw:0"` to your local soundcard number. To list the soundcards type `aplay -l`.
5. After a reboot configuration is done.

```
#!conf
pcm.musicbot_fake_null {
  type plug
  slave.pcm "hw:Loopback,0,1"
  hint {
    show on
    description "Musicbot Fake Null Interface"
    #sends all sounds to hw:Loopback,0,1
}
pcm.musicbot_loop_playback {
  type dmix
  ipc_key 204242
  slave {
    pcm "hw:Loopback,0,0"
    period_time 0
    buffer_time 0
    period_size 2048
    buffer_size 32768
  }
}

pcm.musicbot_sndcard_playback {
  type dmix
  ipc_key 104242
  slave {
    pcm "hw:1"
    period_time 0
    buffer_time 0
    period_size 2048
    buffer_size 32768
  }
}

pcm.musicbot_loop_capture {
  type dsnoop
  ipc_key 304242
  slave.pcm "hw:Loopback,1,0"
}

pcm.musicbot_plug {
  type plug
  slave {
    pcm {
      type asym
      playback.pcm "musicbot_loop_playback"
      capture.pcm "musicbot_loop_capture"
    }
  }
  hint {
    show on
    description "Musicbot Playback Interface (only loopback)"
  }
}

pcm.loop_and_normal {
  type multi
  
  slaves.a.pcm "musicbot_loop_playback"
  slaves.a.channels 2
  slaves.b.pcm "musicbot_sndcard_playback"
  slaves.b.channels 2
 
  bindings.0.slave a
  bindings.0.channel 0
  bindings.1.slave a
  bindings.1.channel 1
  bindings.2.slave b
  bindings.2.channel 0
  bindings.3.slave b
  bindings.3.channel 1
}

pcm.musicbot_playback {
  type plug
  slave.pcm "loop_and_normal"
  route_policy "duplicate"
  hint {
    show on
    description "Musicbot Playback Interface (loopback + normal)"
  }
}

```

With this configuration it is possible to route sound to local speakers and the recording interface or just to the recording interface. In both scenarios the name used for the recording interface is ``musicbot_plug``. To change the recording interface of teamspeak3 go to `Settings->Options->Recording` select `ALSA` in recording mode `custom` in recording device and insert ``musicbot_plug`` in the textfield. Now change the playback interface in `Settings->Options->Playback`. Select `ALSA` as playbackmode and `custom` with devicename `musicbot_fake_null` to discard all sounds played by teamspeak. *[It is not really discarded, but sent to the other Loopback-card. If you choose the real null device teamspeak consumes 100% CPU time of one core]*

In order to route sound to local speakers and recording interface choose in VLC as playback device `Musicbot Playback Interface (loopback + normal)`, otherwise choose `Musicbot Playback Interface (only loopback)`.

####PulseAudio loopback####
1. Install packages `pulseaudio` (audio backend) and `pavucontrol` (control gui)
2. Append to the global config file `/etc/pulse/default.pa` following lines
```
#musicbot null-sink output
load-module module-null-sink sink_name=musicbot_out sink_properties=device.description="musicbot_out"
#musicbot output to card0
load-module module-loopback source=musicbot_out.monitor sink=0
```
3. reboot
4. Start VLC and Teamspeak
5. Select `musicbot_out` as playback device in VLC (`Audio->Audiodevice->musicbot_out`)
6. Start PulseAudio Volume Control (pavucontrol in the terminal)
7. Select PulseAudio in Teamspeak for input and output (`Settings->Options->(Playback|Recording)->(Playback|Recording)mode`)
8. Go to playback tab and mute the output of teamspeak3
9. Go to recording tab and select `Monitor of musicbot_out` as teamspeak input